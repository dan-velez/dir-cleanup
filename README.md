# dir-cleanup
Organize a directory, by answering a prompt for each file. This is useful for organizing large and messy directories.

## Installation
```
npm i -g dir-cleanup
```

## Usage
```
$ dir-cleanup <dir-name>
```

## Options per file
***[K]eep***
> The file stays in the current directory. Nothing is changed.

***[M]ove***
> Move the file to another path. Can be used to rename.

***[D]elete***
> Delete the file. This cannot be undone.

***[E]xit***
> Exit the prompt.
