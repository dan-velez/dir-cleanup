# File Array Prompt
# -----------------
# Prompt the user for a directory, 
# then prompt the user for each filenin that directory.

out = console.log
promptly = require 'promptly'
path = require 'path'
fs = require 'fs'
{ exec } = require 'child_process'

fileArrayPrompt = ->
  q = "Specifiy a location for operations (you are in #{path.resolve '.'}): "
  await promptly.prompt q, defer err, inputPath
  root = ''
  if inputPath.startsWith '/' then root = inputPath
  else root = "#{path.resolve '.'}/#{inputPath}"
  out 'Working in', root
  await fs.readdir root, defer err, files
  if err
    console.error 'Invalid directory!'
    return fileArrayPrompt()
  else
    out 'files are', files
    for file in files
      await filePrompt root, file, defer()

filePrompt = (root, file, callb)->
  q = "Where would you like to move #{file}? [path or 0] "
  q += '(Directories should end in with /):'
  await promptly.prompt q, defer err, userPath
  if userPath is '0' then return callb()
  else
    cmd = "mv #{root}/#{file} #{root}/#{userPath}"
    await exec "mkdir #{root}/#{userPath}", defer()
    await exec cmd, defer err, stderr, stdout
    if err or stderr
      out err, stderr
      return filePrompt root, file, callb
    out stdout if stdout
    out 'mv successful!'
    callb()

fileArrayPrompt()
