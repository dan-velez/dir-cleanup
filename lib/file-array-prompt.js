/* file-array-prompt. */
/* TODO
 * Config file to store dir paths as variables.
 * File organization. What can the code do to improve file org.
 * Implement mkdir -p
 */

const readline = require('readline');
const fs = require('fs');
const prompt = require('prompt-sync')();
const path_mod = require('path');

let dirpath = "";

/* Move to bin/ */
const pkg = require('../package.json');
console.log(`file-array-prompt v${pkg['version']}`);
if(process.argv.length < 3) {
    usage();
}
else {
    dirpath = process.argv[2];
    console.log("__________________________________________\n"+
                "The CLI will now prompt you for each file.\n" +
                "Your options are:\n"+
                "* Keep\n"+
                "* Delete\n"+
                "* Move <New Path>\n"+
                "* exit\n"+
                "Commands are case insensitive.\n"+
                "__________________________________________\n"
               )
    main();
}

function main() {
    /* Run the interactive prompt for each file in the directory. */ 
    console.log("Follow the prompts to organize directory ["+dirpath+"]")
    console.log("You are in "+path_mod.resolve('.'));
    /* Get the files list. */
    let files_list = [];
    fs.readdir(dirpath, (err, files)=> {
        files_list = files;
        /* Run prompt on each file. */
        let counter = 0;
        files_list.map(async function(file) {
            handle_file(file);
        });
    });
}

function handle_file(file) {
    /* Prompt the user for input and perform requested action for file.
     * The I/O operations are synchronous.
     * */
    let file_prompt = (fname)=> {
        return "What would you like to do with ["+dirpath+fname+"]? ";
    }
    let user_resp = prompt(file_prompt(file));
    let file_action = "";
    let action_arg = "";
    let args = [];
    try {
        args = user_resp.trim().split(" ");
        console.log(args);
        file_action = args[0];
        if(user_resp != "keep") {
            action_arg = args[1];
        }
    } catch(err) {
        console.log("Incorrect input format!");
        return handle_file(file);
    }

    if(!file_action) {
        console.log("No command specified!");
        return handle_file(file);
    }

    if(file_action == "move") {
        try {
            fs.renameSync(path_mod.resolve(dirpath+file), path_mod.resolve(action_arg));
        }
        catch(err) {
            console.log("IO error!", err);
            return handle_file(file);
        }
    }
    else if(file_action == "delete") {
        try {
            fs.unlinkSync(path_mod.resolve(dirpath+file));
        }
        catch(err) {
            console.log("IO error!", err);
            return handle_file(file);
        }
    }
    else if(file_action == "keep") {

    }
    else if(file_action == "exit") {
        console.log("Exiting file-array-prompt.");
        process.exit();
    }
    else {
        // Recall the same function.
        console.log("Incorrect input format!");
        return handle_file(file);
    }
}

function usage() {
    console.log("Usage: file-array-prompt <dir-path>\n" +
                "   dir-path: The name of the directory you want to clean up.");
    process.exit();
}

function prompt_user(text) {
    /* Get input from user. Synchronous.*/
    let answer = prompt(text);
    console.log("anwer", answer);
    return anwer;
}

function prompt_user_readline(text) {
    /* Prompt the user for a response. Returns a promise. */
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    return new Promise((resolve, reject)=> {
        rl.question(text, (resp)=> {
            rl.close();
            resolve(resp);
        });
    });
}
